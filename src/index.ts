import express from 'express';
import bodyParser, {json} from 'body-parser';
import mongoose from 'mongoose';
import cors from "cors";
import { adminRouter } from "./routers/admin.router";
//import { collectionsRouter } from "./routes/collections.router";
const path = require("path");
const app = express()
app.use(json())
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', express.static(path.join(__dirname, 'public')));
app.use("/api/admins/v1", adminRouter); 

app.listen(3000, ()=>{
    console.log('server listening on port 3000');
})

export default app;