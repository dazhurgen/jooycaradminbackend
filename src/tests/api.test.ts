import app from '../index';
import * as chai from 'chai';
import chaiHttp = require('chai-http');
chai.use(chaiHttp);
import { request, expect } from 'chai';
import 'mocha';

describe('get admins api', () => {
  it('should return response on call', () => {
    return request(app).get('/api/admins/v1')
      .then(res => {
        expect(res).to.have.status(200);
      })
  })
})


describe('post new admin ', () => {
  it('should create new admin', () => {
    return request(app).post('/api/admins/v1').send({
      name:"Admin",
      lastName:"Test",
      email:'18@jooycar.com',
      role:"admin.fleetr"
    })
      .then(res => {
        expect(res).to.have.status(200);
        expect(res.body).to.be.a('object');
        expect(res.body.status).to.equal('0');  
      })
  })
})

describe('post new admin ', () => {
  it('should fail with 422 if email exists', () => {
    return request(app).post('/api/admins/v1').send({
      name:"Admin",
      lastName:"Test",
      email:'64@jooycar.com',
      role:"admin.fleetr"
    })
      .then(res => {
        expect(res).to.have.status(422);
      })
  })
})


describe('delete admin api with pending status', () => {
  it('should return response on call', () => {
    return request(app).del('/api/admins/v1/6195149041fd90dbf737555f')
      .then(res => {
        expect(res).to.have.status(200);
      })
  })
})

describe('delete fail on admin not pending', () => {
  it('should return response on call', () => {
    return request(app).del('/api/admins/v1/61943fe3ec73a726e9cca04b')
      .then(res => {
        expect(res).to.have.status(422);
      })
  })
})

describe('patch admin success', () => {
  it('should return response on patch', () => {
    return request(app).patch('/api/admins/v1/61943fe3ec73a726e9cca04b').send({
      name:"Admin",
      lastName:"Test xxxx",
      role:"admin.fleetr"
    })
      .then(res => {
        expect(res).to.have.status(200);
      })
  })
})

describe('patch admin email edit fail with status active', () => {
  it('should return 422 response on patch', () => {
    return request(app).patch('/api/admins/v1/61943fe3ec73a726e9cca04b').send({
      name:"Admin",
      lastName:"Test xxxx",
      role:"admin.fleetr",
      email:'admintest45@jooycar.com'
    })
      .then(res => {
        expect(res).to.have.status(422);
      })
  })
})