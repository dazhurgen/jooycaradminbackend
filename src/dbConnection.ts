import mongoose from 'mongoose';
const ObjectId = mongoose.Types.ObjectId;
const conn = mongoose.createConnection('mongodb://localhost:27017/jooycarTestdb',{
});

//we export the conection and also the objectID for later id validation
export {conn, ObjectId};