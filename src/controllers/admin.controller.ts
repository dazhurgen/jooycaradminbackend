import {AdminModel, status} from '../models/admin.model';
import { ObjectId } from '../dbConnection';
import {sendAdminStatusUpdate} from '../controllers/notifications.controller';
export const getAdmins = async function(){

    console.log('requested get Admins');
    return await AdminModel.find({ status: { $not: { $gte: 2 } } }).then(result => {
       if(!result){
           
            console.log(result);
       }
       else{
       
       console.log(result);
       
       }
       return result;
    }).catch(error =>{
       console.log('an error ocurred');
       console.log(error);
       throw(error);
    })
}

export const addAdmins = async function(data : any){
    console.log('requested to add Admins');
    let result =  await  AdminModel.create({name:data.name, 
        lastName:data.lastName, 
        email:data.email, 
        role:data.role, status:status.pending})

        try {
            //send notification to notification api
            let notification = await sendAdminStatusUpdate('send', result._id);
            return result;
        }
        catch(error){
            console.log(error);
            throw(error);
        }
}

export const updateAdmins = async function(id: any, data: any){
    console.log('requested to update admin');
    console.log(data);
    let result = null;

    //validate the mongoose id
    if(isValidObjectId(id)){
        //check if email exist in data passed
        if(data.email ){
            //validate email
            if(isValidEmail(data.email)){
             result = await AdminModel.findOneAndUpdate({_id:id, status:{ $not: { $gt: 0 }}}, data, {
                returnOriginal: false
              });
              
              console.log(result);
              
              if(!result){
                  console.log('invalid params');
                  throw({error:2, message:'user not found or status already active'});
              }
            }
            else{
                throw({error:3, message:'email invalid'});
            }
        }
        else{
             result = await AdminModel.findOneAndUpdate({_id:id}, data, {
                returnOriginal: false
              });
              console.log(result);
        }
       //check if theres a need to send a confirm or reject notification
        switch (data.status){
            case 1:
                console.log('admin has been enabled');
                try {
                    let notification = await sendAdminStatusUpdate('confirm', id);
                    return {result: result, notificationType:'confirm'}
                }
                catch(error){
                    throw(error);
                }
               
            case 2:
                try {
                console.log('admin has been disabled');
                let notification = await sendAdminStatusUpdate('reject', id);
                return {result: result, notificationType:'reject'}
                }
                catch(error){
                    throw(error);
                }
            default:
            console.log('returning result');
            return result;
        }
    } 
    else {
        throw({error:'invalid id', code:1});
    }
    
}
export const deleteAdmin = async function(id: any){
    console.log('requested to delete admin values');
    if(isValidObjectId(id)){
        let result = AdminModel.findOneAndRemove({_id:id, status:{ $not: { $gt: 0 }}}) //only delete admins which status is pending
        //send notification
        let notification = await sendAdminStatusUpdate('reject', id);
        if(!result){
            throw({error:'unable to delete '});
        }
        return result;
    }
    else {
        throw({error:'invalid id', code:1});
    }
}

//id validator
function isValidObjectId(id : any){
    if(ObjectId.isValid(id)){
        if((String)(new ObjectId(id)) === id)
            return true;
        return false;
    }
    return false;
}

//email validator
function isValidEmail(email : any){
    let emailRegex = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
    if (!email)
        return false;

    if(email.length>254)
        return false;

    let valid = emailRegex.test(email);
    if(!valid)
        return false;

    let parts = email.split("@");
    if(parts[0].length>64)
        return false;

    var domainParts = parts[1].split(".");
    if(domainParts.some(function(part:any) { return part.length>63; }))
        return false;

    return true;
}

