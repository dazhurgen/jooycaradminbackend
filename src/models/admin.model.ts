import mongoose from 'mongoose';
import {conn} from '../dbConnection';
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const path = require('path');

enum status {
    pending,
    active,
    disable,
    deleted,
  }

const AdminSchema = new Schema({
    name: {type: String, required:true},
    lastName:{type:String, required:true},
    status:{type:String, required:true, default:status.pending, enum: Object.values(status)},
    email:{type:String, required:true, unique: true, dropDups: true},
    role:{type:String, required:true, default:'admin.fleetr'},
    picture:{type:String, required:true, default:'http://localhost:3000/images/blank_user.png'}
})

const AdminModel = conn.model('Admin', AdminSchema);

export {AdminModel, status}; 