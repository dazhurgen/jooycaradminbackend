import mongoose from 'mongoose';
import {conn} from '../dbConnection';
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const path = require('path');


const NotificationsSchema = new Schema({
    type: {type: String, required:true},
    id:{type:String, required:true}
})

const NotificationsModel = conn.model('Notification', NotificationsSchema);

export {NotificationsModel}; 