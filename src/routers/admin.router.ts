import express, { Request, Response } from "express";
 import * as AdminCtrl from "../controllers/admin.controller";
 export const adminRouter = express.Router();


  adminRouter.get("/", async (req: Request, res: Response) => {
    console.log('should get all admins');
    try{
        let result = await AdminCtrl.getAdmins();
        res.send(result);
    }
    catch(error : any){
        console.log(error);
        res.send(error);
    }
   
  });
  adminRouter.post("/", async (req: Request, res: Response) => {
    console.log('should add a new admin');
    try{
        let result = await AdminCtrl.addAdmins(req.body);
        res.send(result);
    }
    catch(error : any){
        console.log('an error ocurred');
        console.log(JSON.stringify(error));
        if(error.code == 11000){
            if(error.keyPattern.email){
                res.status(422).send({"error": {
                    "statusCode": 0,
                    "errorCode": 0,
                    "srcMessage": "Invalid attribute",
                    "translatedMessage": "Atributo inválido"
                  }})
            }
        }
        if(error.errors){
            res.status(422).send({"error": {
                "statusCode": 0,
                "errorCode": 0,
                "srcMessage": "missing field",
                "translatedMessage": "Campo requerido no existente"
              }})
        }
    }
    
   
  });
  adminRouter.patch("/:id", async (req: Request, res: Response) => {
    try{
        let result = await AdminCtrl.updateAdmins(req.params.id, req.body);
        res.send(result);
    }
   catch(error){
       console.log(error);
    res.status(422).send({"error": {
        "statusCode": 0,
        "errorCode": 0,
        "srcMessage": "Invalid attribute",
        "translatedMessage": "Atributo inválido"
      }})
   }
  });
  adminRouter.delete("/:id", async (req: Request, res: Response) => {
    try{
        let result = await AdminCtrl.deleteAdmin(req.params.id);
        console.log(result);
        if(!result){
            console.log('probably an error with the data');
            res.status(422).send({"error": {
                "statusCode": 0,
                "errorCode": 0,
                "srcMessage": "Invalid attribute",
                "translatedMessage": "Atributo inválido"
              }})
        }
        else{
            res.send(result);
        }
       
    }
    catch(error){
        res.status(422).send({"error": {
            "statusCode": 0,
            "errorCode": 0,
            "srcMessage": "Invalid attribute",
            "translatedMessage": "Atributo inválido"
          }})
    }
  });